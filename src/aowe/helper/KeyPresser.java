package aowe.helper;

import jdk.internal.util.xml.impl.Input;

import java.awt.*;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

/**
 * Created by Igor Farszky on 1.7.2017..
 */
public class KeyPresser {

    Robot bot;

    public KeyPresser() {
        try {
            this.bot = new Robot();
        } catch (AWTException e) {
            System.out.println("FAILED TO INITIALIZE KEY PRESSER");
        }
    }

    public void moveAndclick(int x, int y) {
        move(0, 0);
        move(x, y);
        mousePress(InputEvent.BUTTON1_MASK);
        sleep(100);
    }

    public void sleep(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void mousePress(int key) {
        bot.mousePress(key);
        bot.mouseRelease(key);
    }

    public void keyPress(int key){
        bot.keyPress(key);
        bot.delay(500);
        bot.keyRelease(key);
    }

    public void move(int x, int y) {
        bot.mouseMove(x, y);
    }

    public void click() {
        bot.mousePress(InputEvent.BUTTON1_MASK);
        bot.mouseRelease(InputEvent.BUTTON1_MASK);
        sleep(100);
    }

}
